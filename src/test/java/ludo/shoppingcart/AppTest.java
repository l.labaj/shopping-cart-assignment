package ludo.shoppingcart;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AppTest {

    private static App app;

    @BeforeAll
    static void setup() {
        app = new App();
    }

    @Test
    void checkoutEmpty() {
        var result = app.checkout(List.of());
        assertEquals(BigDecimal.ZERO, result);
    }

    @Test
    void checkout1Apple0Oranges() {
        var result = app.checkout(List.of("apple"));
        assertEquals(new BigDecimal("0.60"), result);
    }

    @Test
    void checkout2Apples0Oranges() {
        var result = app.checkout(List.of("apple", "apple"));
        assertEquals(new BigDecimal("0.60"), result);
    }

    @Test
    void checkout3Apples0Oranges() {
        var result = app.checkout(List.of("apple", "apple", "apple"));
        assertEquals(new BigDecimal("1.20"), result);
    }

    @Test
    void checkout3Apples1Orange() {
        var result = app.checkout(List.of("apple", "apple", "apple", "orange"));
        assertEquals(new BigDecimal("1.45"), result);
    }

    @Test
    void checkout3Apples2Oranges() {
        var result = app.checkout(List.of("apple", "apple", "apple", "orange", "orange"));
        assertEquals(new BigDecimal("1.70"), result);
    }

    @Test
    void checkout3Apples3Oranges() {
        var result = app.checkout(List.of("apple", "apple", "apple", "orange", "orange", "orange"));
        assertEquals(new BigDecimal("1.70"), result);
    }

    @Test
    void checkout3Apples4Oranges() {
        var result = app.checkout(List.of("apple", "apple", "apple", "orange", "orange", "orange", "orange"));
        assertEquals(new BigDecimal("1.95"), result);
    }

    @Test
    void checkout4Apples6Oranges() {
        var result = app.checkout(List.of("apple", "apple", "apple", "apple",
                "orange", "orange", "orange", "orange", "orange", "orange"));
        assertEquals(new BigDecimal("2.20"), result);
    }
}