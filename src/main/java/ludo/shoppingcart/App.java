package ludo.shoppingcart;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class App {

    private final Map<String, BigDecimal> prices = Map.of(
            "apple" , new BigDecimal("0.60"),
            "orange", new BigDecimal("0.25")
    );

    private final Map<String, int[]> offers = Map.of(
            "apple" , new int[] {2, 1},
            "orange", new int[] {3, 2}
    );
    private final int[] noSpecialOffer = new int[] {1, 1};

    public BigDecimal checkout(Collection<String> items) {
        var amounts = items.stream()
                .map(String::toLowerCase)
                .collect(groupingBy(item -> item, counting()));

        return amounts.entrySet().stream()
                .map(entry -> {
                    var item = entry.getKey();
                    var amount = entry.getValue();
                    var offer = offers.getOrDefault(item, noSpecialOffer);
                    var itemsToPayFor = offer[1] * (amount / offer[0]) + amount % offer[0];
                    return prices.get(item).multiply(new BigDecimal(itemsToPayFor));
                }).reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public static void main(String[] args) {
        var app = new App();
        var total = app.checkout(List.of("apple", "Apple", "orange", "APPLE"));
        System.out.println("Total: £" + total);
    }
}
